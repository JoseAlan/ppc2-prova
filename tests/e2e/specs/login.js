describe('Login', () => {
  describe('When visit login url', () => {
    const validEmail = 'email_100@gmail.com';
    const invalidEmail = 'email@gmail.com';
    const validPassword = 'passord100';
    const invalidPassword = 'abc123';
    const passwordNoAccess = 'passord101';
    const emailNoAccess = 'email_101@gmail.com';

    beforeEach(() => {
      cy.visit('/login');
    });

    it('contains h1 Login', () => {
      cy.contains('h1', 'Login');
    });

    describe('When the email is not valid', () => {
      beforeEach(() => {
        cy.get('.form__input_email').type(invalidEmail);
      });
      it('contains a message email error', () => {
        cy.get('.login__h1_email').should('be.visible');
      });
    });

    describe('When the email is valid', () => {
      beforeEach(() => {
        cy.get('.form__input_email').clear().type(validEmail);
      });
      it('the message email error not be visible', () => {
        cy.get('.login__h1_email').should('not.be.visible');
      });
    });

    describe('When the password is not valid', () => {
      beforeEach(() => {
        cy.get('.form__input_password').clear().type(invalidPassword);
      });
      it('contains a message password error', () => {
        cy.get('.password__h1_email').should('be.visible');
      });
    });

    describe('When the password is valid', () => {
      beforeEach(() => {
        cy.get('.form__input_password').clear().type(validPassword);
      });
      it('the message password error not be visible', () => {
        cy.get('.password__h1_email').should('not.be.visible');
      });
    });

    describe('When fill email and password', () => {
      describe('When the user has no access', () => {
        beforeEach(() => {
          cy.get('.form__input_email').clear().type(emailNoAccess);
          cy.get('.form__input_password').clear().type(passwordNoAccess);
          cy.get('button').click();
        });
        it('contains a message wrong login', () => {
          cy.get('.h1__login_wrong').should('be.visible');
        });
      });

      beforeEach(() => {
        cy.get('.form__input_email').clear().type(validEmail);
        cy.get('.form__input_password').clear().type(validPassword);
      });

      describe('When click button login', () => {
        it('should got to user form', () => {
          cy.url().should('eq', 'http://localhost:8080/login');
          cy.get('button').click();
          cy.url().should('eq', 'http://localhost:8080/userForm');
        });
      });
    });
  });
});
