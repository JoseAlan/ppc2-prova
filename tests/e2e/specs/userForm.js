describe('UserForm', () => {
  describe('when userForm url is visited', () => {
    const selectedSo = 'Linux Mint';
    const unSelectedSo = ' ';
    const underAge = '17';
    const overAge = '100';
    const letterInAge = 'a25';
    const correctAge = '18';
    const emailUnformat = 'email_';
    const emailFormat = 'email_100@gmail.com';

    beforeEach(() => {
      cy.visit('userForm');
    });
    it('contains h1 userform', () => {
      cy.contains('h1', 'UserForm');
    });

    describe('When the SO is unselected', () => {
      beforeEach(() => {
        cy.get('select').select(unSelectedSo).should('have.value', unSelectedSo);
      });
      it('contains a message SO unselected', () => {
        cy.get('.form__label_so').should('be.visible');
      });
    });

    describe('When SO is selected', () => {
      beforeEach(() => {
        cy.get('select').select(selectedSo).should('have.value', selectedSo);
      });
      it('the message SO unselected not be visible', () => {
        cy.get('.form__label_so').should('not.be.visible');
      });
    });

    describe('When age is under than 18 years', () => {
      beforeEach(() => {
        cy.get('.form__input_idade').type(underAge);
      });
      it('contains incorrect age message', () => {
        cy.get('.form__h1_idade_incorrect').should('be.visible');
      });
    });

    describe('When age is over than 99 years', () => {
      beforeEach(() => {
        cy.get('.form__input_idade').type(overAge);
      });
      it('contains incorrect age message', () => {
        cy.get('.form__h1_idade_incorrect').should('be.visible');
      });
    });

    describe('When contains a letter in field age', () => {
      beforeEach(() => {
        cy.get('.form__input_idade').type(letterInAge);
      });
      it('contains incorrect age message', () => {
        cy.get('.form__h1_idade_incorrect').should('be.visible');
      });
    });

    describe('When the age is valid', () => {
      beforeEach(() => {
        cy.get('.form__input_idade').type(correctAge);
      });
      it('the incorrect age message not be visible', () => {
        cy.get('.form__h1_idade_incorrect').should('not.be.visible');
      });
    });

    describe('When the email is not valid', () => {
      beforeEach(() => {
        cy.get('.form__input_email').type(emailUnformat);
      });
      it('contains a message email error', () => {
        cy.get('.form__h1_email_unformated').should('be.visible');
      });
    });
  });
});
