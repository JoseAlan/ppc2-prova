import { shallowMount } from '@vue/test-utils';
import UserForm from '@/components/UserForm.vue';

describe('UserForm', () => {
  it('is a valid component ', () => {
    const wrapper = shallowMount(UserForm);
    expect(wrapper.exists()).toBe(true);
  });
  it("should has h1 equals to 'Userform'", () => {
    const wrapper = shallowMount(UserForm);
    expect(wrapper.find('h1').text()).toMatch('UserForm');
  });
});

describe('Userform Validation', () => {
  it('is Empty Field', () => {
    const wrapper = shallowMount(UserForm);
    expect(wrapper.vm.isEmptyField(' ')).toBe(true);
  });
  it('is not Empty Field', () => {
    const wrapper = shallowMount(UserForm);
    expect(wrapper.vm.isEmptyField(' sff ')).toBe(false);
  });
  it('when fields are correct', () => {
    const wrapper = shallowMount(UserForm, {
      computed: { isCompleted: () => true },
    });
    expect(wrapper.find('.form__congratulations').attributes('style')).not.toBe('display: none;');
  });
  it('when fields are incorrect', () => {
    const wrapper = shallowMount(UserForm, {
      computed: { isCompleted: () => false },
    });
    expect(wrapper.find('.form__congratulations').attributes('style')).toBe('display: none;');
  });
  describe('Userform -> Age Field', () => {
    it('should has valid age', () => {
      const wrapper = shallowMount(UserForm);
      expect(wrapper.vm.isValidAge('18')).toBe(true);
    });
    it('should has not a valid age', () => {
      const wrapper = shallowMount(UserForm);
      expect(wrapper.vm.isValidAge('17')).toBe(false);
    });
    it('should has a correct age', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { correctAge: () => true },
      });
      expect(wrapper.find('.form__h1_idade_incorrect').attributes('style')).not.toBe('display: none;');
    });
    it('should has a incorrect age', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { correctAge: () => false },
      });
      expect(wrapper.find('.form__h1_idade_incorrect').attributes('style')).toBe('display: none;');
    });
  });
  describe('Userform -> Email Field', () => {
    it('should has a correct id email ', () => {
      const wrapper = shallowMount(UserForm);
      wrapper.setData({ userID: '100' });
      expect(wrapper.vm.isEmailAuthentic('email_100@gmail.com')).toBe(true);
    });
    it('should has a incorrect id email ', () => {
      const wrapper = shallowMount(UserForm);
      wrapper.setData({ userID: '10' });
      expect(wrapper.vm.isEmailAuthentic('email_100@gmail.com')).toBe(false);
    });
    it('should has unformat message ', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { emailFormat: () => true },
      });
      expect(wrapper.find('.form__h1_email_unformated').attributes('style')).not.toBe('display: none;');
    });
    it('should has not unformat message ', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { emailFormat: () => false },
      });
      expect(wrapper.find('.form__h1_email_unformated').attributes('style')).toBe('display: none;');
    });
  });
  describe('Userform -> SO Field', () => {
    it('When SO is selected', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { isSoSelected: () => true },
      });
      expect(wrapper.find('.form__label_so').attributes('style')).not.toBe('display: none;');
    });
    it('When SO is not selected', () => {
      const wrapper = shallowMount(UserForm, {
        computed: { isSoSelected: () => false },
      });
      expect(wrapper.find('.form__label_so').attributes('style')).toBe('display: none;');
    });
  });
});
