import { shallowMount } from '@vue/test-utils';
import Login from '@/components/Login.vue';

describe('Login', () => {
  it('is a valid component ', () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.exists()).toBe(true);
  });
  it("should has h1 equals to 'Login'", () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.find('h1').text()).toMatch('Login');
  });
});

describe('Login Validations', () => {
  describe('Email and Password Field Validation', () => {
    it('is Empty Field', () => {
      const wrapper = shallowMount(Login);
      expect(wrapper.vm.isEmptyField(' ')).toBe(true);
    });
    it('is not Empty Field', () => {
      const wrapper = shallowMount(Login);
      expect(wrapper.vm.isEmptyField('s ')).toBe(false);
    });
    describe('Should Has Login Access', () => {
      it('should has a userForm valid', () => {
        const wrapper = shallowMount(Login);
        expect(wrapper.vm.userFormValidator('email_1@gmail.com', 'passord1')).toBe(true);
      });
      it('should has not a userForm valid', () => {
        const wrapper = shallowMount(Login);
        expect(wrapper.vm.userFormValidator('email_0@gmail.com', 'passord0')).toBe(false);
      });
      it('should has a userForm fields are differents', () => {
        const wrapper = shallowMount(Login);
        expect(wrapper.vm.userFormValidator('email_100@gmail.com', 'passord1')).toBe(false);
      });
    });
  });
  describe('Login -> Password Field', () => {
    it('should has a valid password ', () => {
      const wrapper = shallowMount(Login);
      expect(wrapper.vm.isValidPassword('passord1')).toBe(true);
    });
    it('should password has size smaller then seven characters ', () => {
      const wrapper = shallowMount(Login);
      expect(wrapper.vm.isValidPassword('abc123')).toBe(false);
    });
  });
  describe('Login -> Email Field', () => {
    it('should has not a valid email ', () => {
      const wrapper = shallowMount(Login);
      expect(wrapper.vm.isEmailAdress('email_100@gmailcom')).toBe(false);
    });
    it('should has a valid email ', () => {
      const wrapper = shallowMount(Login);
      expect(wrapper.vm.isEmailAdress('email_100@gmail.com')).toBe(true);
    });
    describe('When Email Field is Valid', () => {
      const wrapper = shallowMount(Login, {
        computed: { emailFormat: () => true },
      });
      it('should have a email message', () => {
        expect(wrapper.find('.login__h1_email').attributes('style')).not.toBe('display: none;');
      });
    });
    describe('When Email Field is not Valid', () => {
      const wrapper = shallowMount(Login, {
        computed: { emailFormat: () => false },
      });
      it('should have not a email message', () => {
        expect(wrapper.find('.login__h1_email').attributes('style')).toBe('display: none;');
      });
    });
  });
});
