# Pos-test - avaliação da diciplina de Programação Para Cliente 2

## Project progress
> ## José Alan
- [x] Criar componente Login (com testes);
- [x] Criar componente Login (com testes);
- [x] Configurar CI com testes Unitário, e2e e lint;
##### OBS: Falta o teste e2e do campo email, no caso de quando o e-mail é igual ao e-mail do Login.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
